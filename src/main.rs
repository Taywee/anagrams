extern crate permutohedron;
extern crate clap;

use clap::{App, Arg};
use std::io::{stdin, BufReader, BufRead};
use std::fs::File;
use std::collections::HashMap;
use std::collections::HashSet;
use permutohedron::LexicalPermutation;

/** Generate all splits as a vector of slices for string.  Each slice is sorted.
 * Recursive helper function.
 * The collection has to contain vectors, not sets, because repeats should be maintained.  We sort the
 * stack to remove duplicates.
 */
fn get_all_sorted_splits_r(left: &[char], mut stack: Vec<String>, collection: &mut HashSet<Vec<String>>) {
    // For all except the full-length, because it should be recursive, and collection should only
    // be inserted to for the top-length case
    for i in (1..left.len()).rev() {
        // Split this subsection, setting the new left bit (which is actually the right of the
        // split) and pushing the rest onto the stack
        let new_left = &left[i..];
        let mut top_vec: Vec<_> = left[..i].iter().cloned().collect();
        top_vec.sort_unstable();
        let top = top_vec.iter().collect();
        let mut new_stack = stack.clone();
        new_stack.push(top);
        get_all_sorted_splits_r(new_left, new_stack, collection);
    }

    // Push the entire slice onto the stack and push it into the collection
    let mut top_vec: Vec<_> = left.iter().cloned().collect();
    top_vec.sort_unstable();
    let top = top_vec.iter().collect();
    stack.push(top);
    stack.sort_unstable();
    collection.insert(stack);
}

/** Generate all splits as a vector of slices for string.  Each split is fully sorted for
 * convenience.
 */
fn get_all_sorted_splits(string: &[char]) -> HashSet<Vec<String>> {
    let mut output = HashSet::new();
    get_all_sorted_splits_r(string, Vec::new(), &mut output);
    output
}

/** Generate all splits of all permutations as a vector of slices for string.  Each split is fully sorted for
 * convenience.
 */
fn get_all_permuted_sorted_splits(string: &String) -> HashSet<Vec<String>> {
    let mut permutation: Vec<_> = string.chars().filter(|c| c.is_alphabetic()).collect();
    permutation.sort_unstable();
    let mut output = HashSet::new();
    loop {
        output.extend(get_all_sorted_splits(&permutation));
        if !permutation.next_permutation() {
            break;
        }
    }
    output
}

fn main() {
    let args = App::new("anagrams")
        .version("0.1.0")
        .about("Calculates anagrams from input text")
        .author("Taylor C. Richberger")
        .arg(Arg::with_name("dictionary")
             .help("Input dictionary file.  Defaults to stdin, which can be explicitly specified with -")
             .short("d")
             .long("dictionary")
             .value_name("DICTIONARY")
             .takes_value(true))
        .arg(Arg::with_name("INPUT")
             .help("Word/phrase to find anagrams for.")
             .required(true)
             .index(1))
        .get_matches();

    eprintln!("Building dictionary");

    // Build the dictionary, which is a set of sorted strings to a set of the strings they
    // represent
    let dictionary = {
        let mut dictionary: HashMap<String, HashSet<String>> = HashMap::new();

        let dictionary_filename = args.value_of("dictionary").unwrap_or("-");
        let dictionary_stream: Box<dyn BufRead> = if dictionary_filename == "-" {
            Box::new(BufReader::new(stdin()))
        } else {
            Box::new(BufReader::new(File::open(&dictionary_filename).unwrap()))
        };
        for line in dictionary_stream.lines() {
            let word = line.unwrap().to_lowercase();
            let mut chars: Vec<_> = word.chars().collect();
            chars.sort_unstable();
            let sorted_word: String = chars.iter().filter(|c| c.is_alphabetic()).collect();
            if let Some(set) = dictionary.get_mut(&sorted_word) {
                set.insert(word);
            } else {
                let mut set = HashSet::new();
                set.insert(word);
                dictionary.insert(sorted_word, set);
            }
        }
        dictionary
    };

    let input = args.value_of("INPUT").unwrap().to_lowercase();
    eprintln!("Permuting input and processing splits");

    // This is a little ugly, but it is the core of the processing
    let mut anagrams: Vec<Vec<Vec<String>>> = get_all_permuted_sorted_splits(&input).iter().filter_map(|splits| {
        // Gets all splits of all permutations of the input
        let anagram: Option<Vec<&HashSet<String>>> = splits.iter().map(|split| dictionary.get(split)).collect();
        if let Some(anagram) = anagram {
            let mut processed: Vec<Vec<String>> = anagram.iter().map(|slice| {
                let mut sorted: Vec<String> = slice.iter().cloned().collect();
                sorted.sort_unstable();
                sorted
            }).collect();
            processed.sort_unstable();
            Some(processed)
        } else {
            None
        }
    }).collect();

    eprintln!("Sorting {} anagrams", anagrams.len());

    anagrams.sort_unstable_by(|a, b| a.len().cmp(&b.len()));

    for anagram in anagrams {
        println!("{:?}", anagram);
    }
}
